package comsoc

import (
  "testing";
  //  "fmt";
  //"reflect"
  rad "gitlab.utc.fr/lagruesy/restagentdemo";
)

func TestSTV_SWF(t *testing.T){
  var prof [][]rad.Alternative
  voter1 := []rad.Alternative{1,2,3}
  voter2 := []rad.Alternative{2,3,1}
  voter3 := []rad.Alternative{1,2,3}
  prof = append(prof, voter1, voter2, voter3)
  got, _ := STV_SWF(prof)
  want := map[rad.Alternative]int{
    1: 2,
}
  for alt, val := range(got){
    if val != want[alt]{
      t.Errorf("On a %d alors que l'on veut %d etant donne, %v", got, want, prof)
    }
  }
}
