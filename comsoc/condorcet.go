package comsoc

import (
	"fmt"

	rad "gitlab.utc.fr/lagruesy/restagentdemo"
	//"fmt"
)

func CondorcetWinner(prefs rad.Profile) rad.Alternative {
	var arr_candidats []rad.Alternative
	couple_candidats := make(map[string]int)
	score_candidats := make(map[rad.Alternative]int)
	var iter_cand rad.Alternative
	var sum_iter_cand int
	// initialisation de l'array contenant toutes les alternatives
	for _, alt := range prefs[0] {
		score_candidats[alt] = 0
		arr_candidats = append(arr_candidats, alt)
	}
	// Comparaison deux à deux des candidats
	for len(arr_candidats) != 1 {
		iter_cand = arr_candidats[0]
		arr_candidats = arr_candidats[1:]
		for _, alt2 := range arr_candidats {
			// on evite les doublons, AB = BA ici 
			couple_considere1 := fmt.Sprintf("%d%d", iter_cand, alt2)
			couple_considere2 := fmt.Sprintf("%d%d", alt2, iter_cand)
			_, ok1 := couple_candidats[couple_considere1]
			_, ok2 := couple_candidats[couple_considere2]
			if ok1 || ok2 {
				continue
			} else {
				// on regarde array par array qui est pref
				sum_iter_cand = 0
				for _, tab := range prefs {
					sum_iter_cand += pref_alt(iter_cand, alt2, tab)
				}
				if sum_iter_cand > 0 {
					score_candidats[iter_cand] += 1
				} else {
					score_candidats[alt2] += 1
				}
			}
		}
	}
	condorcet_winner := GetMaxOfMap(score_candidats)
	return condorcet_winner
}

func GetMaxOfMap(p map[rad.Alternative]int) rad.Alternative {
	max := 0
	var bestalt rad.Alternative
	for alt, score := range p {
		if score > max {
			bestalt = alt
			max = score
		}
	}
	return bestalt
}
