package main

import (
	"log"

	"gitlab.utc.fr/lagruesy/restagentdemo/restclientagent"
	"gitlab.utc.fr/lagruesy/restagentdemo/restserveragent"
)

func main() {
	const url1 = ":8000"                 //Serveur port
	const url2 = "http://localhost:8000" //Server URL

	//Creating server
	servAgt := restserveragent.NewRestServerAgent(url1)
	//Creating God
	godAgt := restclientagent.NewGod("Zeus", url2)

	//Launching server
	log.Println("Starting server...")
	go servAgt.Start()

	//Launching God
	log.Println("Starting God...")
	godAgt.Start()
}
