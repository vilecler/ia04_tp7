package comsoc

import (
  "testing";

  rad "gitlab.utc.fr/lagruesy/restagentdemo";
)

func TestCondorcetWinner(t *testing.T){
  //func BordaSWF(p Profile) (Count, error){
  var prof [][]rad.Alternative
  voter1 := []rad.Alternative{1,2,3}
  voter2 := []rad.Alternative{2,3,1}
  voter3 := []rad.Alternative{1,2,3}
  prof = append(prof, voter1, voter2, voter3)
  got := CondorcetWinner(prof)
  want := rad.Alternative(1)
  if got != want {
      t.Errorf("On a %d alors que l'on veut %d etant donne, %v", got, want, prof)
    }
}
