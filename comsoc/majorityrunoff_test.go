package comsoc

import (
  "testing";
  "reflect";

  rad "gitlab.utc.fr/lagruesy/restagentdemo";
)

func TestMajorityRunOffSWF(t *testing.T){
  //func BordaSWF(p Profile) (Count, error){
  var prof [][]rad.Alternative
  voter1 := []rad.Alternative{1,2,3}
  voter2 := []rad.Alternative{2,3,1}
  voter3 := []rad.Alternative{1,2,3}
  voter4 := []rad.Alternative{3,1,2}
  voter5 := []rad.Alternative{1,3,2}
  voter6 := []rad.Alternative{2,1,3}
  prof = append(prof, voter1, voter2, voter3, voter4,voter5,voter6)
  got, _ := MajorityRunOffSWF(prof)
  want := map[rad.Alternative]int{
    1: 4,
    2: 2,
    3: 0,
}
  for alt, val := range(got){
    if val != want[alt]{
      t.Errorf("On a %d alors que l'on veut %d etant donne, %v", got, want, prof)
    }
  }
}

func TestMajorityRunOffSCF(t *testing.T){
  var prof [][]rad.Alternative
  voter1 := []rad.Alternative{1,2,3}
  voter2 := []rad.Alternative{2,3,1}
  voter3 := []rad.Alternative{1,2,3}
  voter4 := []rad.Alternative{3,1,2}
  voter5 := []rad.Alternative{1,3,2}
  voter6 := []rad.Alternative{2,1,3}
  prof = append(prof, voter1, voter2, voter3, voter4,voter5,voter6)
  got, _ := MajorityRunOffSCF(prof)
  want := []rad.Alternative{1}
  if !( reflect.DeepEqual(got, want)) {
    t.Errorf("On a %d  alors que l'on veut %d etant donne, %v", got, want,   prof)
  }
}
