package restclientagent

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strings"
	"sync"
	"time"

	rad "gitlab.utc.fr/lagruesy/restagentdemo"
	comsoc "gitlab.utc.fr/lagruesy/restagentdemo/comsoc"
)

//God struct
type God struct {
	id  string //Id of God
	url string
}

//God constructor
func NewGod(id string, url string) *God {
	return &God{id, url}
}

//Getting the vote ID
func (rca *God) treatResponsePost(r *http.Response) string {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)

	var resp rad.ResponsePost
	json.Unmarshal(buf.Bytes(), &resp)

	return resp.ID
}

/*
 *  Fonction createVote
 *
 *	Permet de créer un vote en envoyant une requête selon les paramètres de l'utilisateur.
 *  Crée également des agents pour voter. Cette fonction attend que les votersNumber Agents aient voté pour se terminer
 */
func (rca *God) createVote(prenoms []comsoc.Prenom, votingType string, alternativesNumber int, votersNumber int) {
	alternatives := make([]rad.Alternative, alternativesNumber) //Make a list of alternatives

	for i := 0; i < alternativesNumber; i++ { //Fill the list in ordered order
		alternatives[i] = rad.Alternative(i)
	}

	res, _ := rca.doRequest(votingType, alternatives, votersNumber) //Call server to create vote
	fmt.Println("A new vote has been created with ID:", res)        //Show vote ID

	time.Sleep(time.Second)
	fmt.Println()
	fmt.Println("Generating Agents...")
	time.Sleep(time.Second)

	var wg sync.WaitGroup
	agts := GenerateAgents(prenoms, votersNumber, rca.url, &wg) //Generate votersNumber Agents
	wg.Add(len(agts))

	//Starting agents
	for _, agt := range agts { //For each agent
		//Warning, must use this lambda functiont to capture iteration value through goroutines
		func(agt RestClientAgent) {
			go agt.Start() //Run agent
		}(agt)
	}

	fmt.Println("Agents are voting. Waiting for them to finish...")
	wg.Wait() //While agent are voting...
	fmt.Println()
	fmt.Println("All agents have voted. Let's get the results...")

	bestAlternatives := rca.getVote(votingType, res) //Getting vote results
	fmt.Println("Vote results:", bestAlternatives)
}

/*
 *  Fonction doRequest
 *
 *	Permet d'envpyer la requête pour créer un nouveau vote.
 *  Renvoie l'identifiant du vote ou une erreur.
 */
func (rca *God) doRequest(votingType string, alternatives []rad.Alternative, votersNumber int) (res string, err error) {
	req := rad.RequestPost{ //Request object
		VotersNumber: votersNumber,
		Alternatives: alternatives,
	}

	votingTypeURL := strings.ReplaceAll(strings.ToLower(votingType), " ", "")
	url := rca.url + "/" + votingTypeURL + "/create" //Request url
	data, error := json.Marshal(req)
	if error != nil {
		fmt.Println("Marshal error", error)
	}

	resp, err := http.Post(url, "application/json", bytes.NewBuffer(data)) //Sending request

	//Handle response
	if err != nil {
		fmt.Println("Error while getting vote id", err)
		return
	}
	if resp.StatusCode != http.StatusOK {
		err = fmt.Errorf("[%d] %s", resp.StatusCode, resp.Status)
		return
	}
	res = rca.treatResponsePost(resp) //Getting the vote ID
	return
}

/*
 *  Fonction getVote
 *
 *	Permet de récupérer les résultats d'un vote en fonction de son ID.
 *  Retourne les résultats du vote sous la forme d'une liste d'alternatives de taille 1.
 */
func (rca *God) getVote(votingType string, voteID string) []rad.Alternative {
	votingTypeURL := strings.ReplaceAll(strings.ToLower(votingType), " ", "")
	url := rca.url + "/" + votingTypeURL + "/results?vote_id=" + voteID //request url

	resp, err := http.Get(url) //sending request

	//Handle response
	if err != nil {
		return nil
	}
	if resp.StatusCode != http.StatusOK {
		err = fmt.Errorf("[%d] %s", resp.StatusCode, resp.Status)
		return nil
	}

	buf := new(bytes.Buffer)
	buf.ReadFrom(resp.Body)

	var result rad.ResponseGet
	json.Unmarshal(buf.Bytes(), &result) //Reading results

	return result.Alternatives
}

//Asks user number of alternatives
func askUserAlternativesNumber() int {
	n := -1
	fmt.Println("Enter n, the number of alternatives (0 to exit): ")
	fmt.Scan(&n)
	return n
}

//Asks user number of voters
func askUserVotersNumber() int {
	n := -1
	fmt.Println("Enter n, the number of voters (0 to exit): ")
	fmt.Scan(&n)
	return n
}

//Asks user which algorithm he wants to use
func askUserAlgorithm() int {
	var n int
	fmt.Println("Which algorithm do you want to use?")

	var i int
	types := rad.GetSupportedTypes()

	for ; i < len(types); i++ {
		fmt.Println(i, "-", types[i], "Algorithm")
	}

	fmt.Println(i, "- Exit program")

	fmt.Scan(&n)

	return n
}

/*
 *  Fonction Start
 *
 *	Permet de démarrer Dieu. Celui ci demande en boucle à l'utilisateur quels paramètres il veut pour créer un nouveau vote.
 *
 */
func (rca *God) Start() {
	log.Printf("Démarrage de %s 🔱\n", rca.id)
	fmt.Println("Loading prenoms from 'Prenoms.csv'. This may take a while...")
	prenoms := comsoc.ExtractPrenoms() //Extracting prenoms from Prenoms.csv. This may take a while.

	for { //Always ask user what he wants to do
		fmt.Println()
		fmt.Println("Let's start a new vote!")
		altNumber := askUserAlternativesNumber() //Choose number of Alternatives
		for altNumber < 0 {
			fmt.Println("It must be a positive number.")
			altNumber = askUserAlternativesNumber()
		}
		if altNumber == 0 { //stop program
			break
		}

		n := askUserVotersNumber() //Choose number of voters
		for n < 0 {
			fmt.Println("It must be a positive number.")
			n = askUserVotersNumber()
		}
		if n == 0 { //stop program
			break
		}

		choice := askUserAlgorithm() //Choose Algorithm
		for choice < 0 || choice > len(rad.GetSupportedTypes())+1 {
			fmt.Println("You must enter a number between 0 and ", len(rad.GetSupportedTypes())+1)
			choice = askUserAlgorithm()
		}

		if choice == len(rad.GetSupportedTypes()) { //stop program
			break
		}

		rca.createVote(prenoms, rad.GetSupportedTypes()[choice], altNumber, n) //Creating a new vote with what user have choosen
	}
	fmt.Println("Bye.") //End of program
}
