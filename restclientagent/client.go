package restclientagent

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"math/rand"
	"net/http"
	"strconv"
	"strings"
	"sync"
	"time"

	rad "gitlab.utc.fr/lagruesy/restagentdemo"
	comsoc "gitlab.utc.fr/lagruesy/restagentdemo/comsoc"
)

//RestClientAgent struct
type RestClientAgent struct {
	ID    rad.AgentID
	Name  string
	Prefs []rad.Alternative
	url   string
	wg    *sync.WaitGroup
}

//RestClientAgent constructor
func NewRestClientAgent(id rad.AgentID, name string, url string, wg *sync.WaitGroup) *RestClientAgent {
	return &RestClientAgent{id, name, []rad.Alternative{}, url, wg}
}

func (ag1 RestClientAgent) Equal(ag2 RestClientAgent) bool {
	if ag1.ID != ag2.ID {
		return false
	}

	return true
}

func (ag1 RestClientAgent) DeepEqual(ag2 RestClientAgent) bool {
	if ag1.ID != ag2.ID {
		return false
	}

	if ag1.Name != ag2.Name {
		return false
	}

	if len(ag1.Prefs) != len(ag2.Prefs) {
		return false
	}

	for i := range ag1.Prefs {
		if ag1.Prefs[i] != ag1.Prefs[i] {
			return false
		}
	}

	return true
}

func (a RestClientAgent) Clone() *RestClientAgent {
	return NewRestClientAgent(a.ID, a.Name, a.url, a.wg)
}

func (a RestClientAgent) String() string {
	return fmt.Sprintf("%d %s %v", int(a.ID), a.Name, a.Prefs)
}

func (a RestClientAgent) rank(b rad.Alternative) (int, error) {
	for i, v := range a.Prefs {
		if v == b {
			return i, nil
		}
	}
	return -1, errors.New("Alternative not found")
}

// renvoie vrai si ag préfère a à b
func (ag RestClientAgent) Prefers(a, b rad.Alternative) bool {
	r1, err1 := ag.rank(a)
	if err1 != nil {
		return false
	}

	r2, err2 := ag.rank(b)
	if err2 != nil {
		return false
	}

	return r1 < r2
}

//Genarate preferences randomly over Alternatives
func RandomPrefs(ids []rad.Alternative) (res []rad.Alternative) {
	res = make([]rad.Alternative, len(ids))
	copy(res, ids)
	rand.Shuffle(len(res), func(i, j int) { res[i], res[j] = res[j], res[i] })
	return
}

//Tells if Agent can vote for a determined ProtectedVote
func (ag *RestClientAgent) canVote(votes []rad.ProtectedVote) bool {
	for i := 0; i < len(votes); i++ {
		if votes[i].Open == 1 {
			return true
		}
	}
	return false
}

/*
 *  Fonction getAllVotes
 *
 *	Permet de récupérer depuis le serveur l'ensemble des votes qu'il existe. Il s'agit de votes de type ProtectedVote pour rendre incaccessible les préférences des autres votants
 *
 */
func (ag *RestClientAgent) getAllVotes() []rad.ProtectedVote {
	url := ag.url + "/all?ID=" + strconv.Itoa(int(ag.ID)) //URL for getting all votes
	resp, err := http.Get(url)                            //Sending request

	//Handle response
	if err != nil {
		fmt.Println("An error occurred while retrieving all votes.", err)
		return nil
	}
	if resp.StatusCode != http.StatusOK {
		err = fmt.Errorf("[%d] %s", resp.StatusCode, resp.Status)
		return nil
	}

	buf := new(bytes.Buffer)
	buf.ReadFrom(resp.Body)

	var result rad.ResponseAll
	json.Unmarshal(buf.Bytes(), &result) //Reading result

	return result.Votes
}

/*
 *  Fonction Start
 *
 *	Permet de démarrer l'Agent. Celui ci récupère depuis le serveur (/all GET) les votes. Il vote aux votes disponibles en générant des préférences aléatoires.
 *	L'agent meurt lorsque plus aucun vote n'est disponible.
 *
 */
func (ag *RestClientAgent) Start() {
	votes := ag.getAllVotes() //Get all votes openned

	fmt.Println(ag.Name, "has been created 👶")

	for ag.canVote(votes) { //While there is a vote still openned
		for i := 0; i < len(votes); i++ { //For each vote
			if votes[i].HasVoted == 0 { //Agent has not voted yet
				ag.Prefs = RandomPrefs(votes[i].Alternatives) //Ranfom preferences generated

				if votes[i].Type == "Approval" { //Cas spécial pour Approval l'agent ne donne pas toutes ses préférences
					ag.Prefs = ag.Prefs[0:rand.Intn(len(ag.Prefs))]
				}

				req := rad.RequestPut{ //creating request object
					ID:     ag.ID,
					Prefs:  ag.Prefs,
					VoteID: votes[i].ID,
				}

				voteTypeURL := strings.ReplaceAll(strings.ToLower(votes[i].Type), " ", "") //where to vote
				url := ag.url + "/" + voteTypeURL + "/vote"
				data, _ := json.Marshal(req)

				obj, err := http.NewRequest(http.MethodPut, url, bytes.NewBuffer(data)) //Constructing request HTTP
				if err != nil {
					fmt.Println("An error occured while trying to vote", err)
				}

				obj.Header.Set("Content-Type", "application/json; charset=utf-8") //Adding header to HTTP request
				client := &http.Client{}
				resp, err := client.Do(obj) //Sending HTTP request
				if err != nil {
					panic(err)
				}

				buf := new(bytes.Buffer)
				buf.ReadFrom(resp.Body)

				var result rad.ResponsePut
				json.Unmarshal(buf.Bytes(), &result) //Getting response from server : 1 vote accepted, 0 vote refused

				// Handling response
				if result.Result == 1 {
					fmt.Println("I am", ag.Name, "(", strconv.Itoa(int(ag.ID)), ") and I have voted to", votes[i].ID)
				} else {
					fmt.Println("I am", ag.Name, "(", strconv.Itoa(int(ag.ID)), ") and I was not able to vote to", votes[i].ID)
				}
			}
		}

		time.Sleep(time.Second) //Just wait one second
		votes = ag.getAllVotes()
	}

	fmt.Println(ag.Name, "is dead. Rest In Peace 💀") //Agent is dead now
	ag.wg.Done()
}

/*
 *  Fonction GenerateAgents
 *
 *	Permet de générer les deux groupes d'agents à partir d'une liste de prénom et d'une taille n.
 *
 */
func GenerateAgents(p []comsoc.Prenom, n int, url string, wg *sync.WaitGroup) (agents []RestClientAgent) {
	randomPrenoms := comsoc.ShufflePrenoms(p, n) //Get a list of prenoms

	agents = make([]RestClientAgent, 0, n) //Make a list of Agent

	for i := 0; i < n; i++ { //generating agent
		agent := *NewRestClientAgent(rad.AgentID(i), randomPrenoms[i].Prenom, url, wg) //Creating agent object
		agents = append(agents, agent)
	}
	return agents
}
