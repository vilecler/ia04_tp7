package main

import (
	"sync"

	"gitlab.utc.fr/lagruesy/restagentdemo/restclientagent"
)

func main() {
	var wg sync.WaitGroup
	wg.Add(1)
	ag := restclientagent.NewRestClientAgent(0, "Robert", "http://localhost:8000", &wg)
	ag.Start()
}
