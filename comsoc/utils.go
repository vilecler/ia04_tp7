package comsoc

import (
	"errors"
	"fmt"

	rad "gitlab.utc.fr/lagruesy/restagentdemo"
)

/*
 * Fonction rank
 *
 * Retourne le classement de l'alternative dans l'esemble des préférences (de 0 à n)
 * Retourne -1 si l'alternative n'est pas
 */
func rank(alt rad.Alternative, prefs []rad.Alternative) int {
	for i := 0; i < len(prefs); i++ {
		if prefs[i] == alt {
			return i
		}
	}
	return -1
}

func GetMaxOfCount(c rad.Count) rad.Alternative {
	max := 0
	var bestalt rad.Alternative
	for alt, score := range c {
		if score > max {
			bestalt = alt
			max = score
		}
	}
	return bestalt
}

func GetMinOfCount(c rad.Count) rad.Alternative {
	min := 99999999 //math.inf(1) not working
	var worst_alt rad.Alternative
	for alt, score := range c {
		if score < min {
			worst_alt = alt
			min = score
		}
	}
	return worst_alt
}

func UpdateProfile(p rad.Profile, alt rad.Alternative) rad.Profile {
	var new_prof rad.Profile
	for _, prof := range p {
		indice := 0
		for i, alt2 := range prof {
			if alt == alt2 {
				indice = i
			}
		}
		new_prof = append(new_prof, append(prof[:indice], prof[indice+1:]...))
	}
	return new_prof
}

func pref_alt(alt1 rad.Alternative, alt2 rad.Alternative, tab []rad.Alternative) int {
	var nb int
	if alt1 == alt2 {
		return 0
	}
	for _, val := range tab {
		if val == alt1 {
			nb = 1
			break
		}
		if val == alt2 {
			nb = -1
			break
		}
	}
	return nb
}

func maxCount(counts map[rad.Alternative]int) (bestAlts []rad.Alternative) {
	var BestA []rad.Alternative
	max := 0
	for len(counts) != 0 {
		for alternat, score := range counts {
			if score > max {
				max = score
				BestA = []rad.Alternative{alternat}
				delete(counts, alternat)
			} else if score == max {
				BestA = append(BestA, alternat)
				delete(counts, alternat)
			} else {
				delete(counts, alternat)
			}
		}
	}
	return BestA
}

/*
 * Fonction checkPrefs
 *
 *
 */
func checkProfile(prefs rad.Profile) error {
	if len(prefs) <= 0 {
		return errors.New("Profile is empty")
	}
	n := len(prefs[0])
	for i := 0; i < len(prefs); i++ {
		if len(prefs[i]) != n {
			return errors.New("Preferences sizes are not equals")
		}

		for j := 0; j < len(prefs[i]); j++ { //on vérifie si des alternatives sont présentes deux fois
			c := 0
			for k := 0; k < len(prefs[i]); k++ {
				if i != k && prefs[i][j] == prefs[i][k] {
					c++
					if c >= 2 {
						return errors.New("Alternative must be chosen once at maximum by agent")
					}
				}
			}
		}
	}
	return nil
}

func checkProfileAlternative(prefs rad.Profile, alts []rad.Alternative) error {
	if len(prefs) <= 0 {
		return errors.New("Profile is empty")
	}

	if len(prefs[0]) != len(alts) {
		return errors.New("Each alternative must be present once")
	}

	return checkProfile(prefs)
}

func Test() {
	fmt.Println("Test")

	alternatives := []rad.Alternative{2, 6, 5, 71, 156, 42, 17, 26}

	counts := make(rad.Count)

	counts[2] = 0
	counts[156] = 4
	counts[5] = 1
	counts[17] = 15
	counts[26] = 4

	fmt.Println(rank(-1, alternatives))
	fmt.Println(maxCount(counts))
}
