package comsoc

import (
  rad "gitlab.utc.fr/lagruesy/restagentdemo";
  //  "fmt"
  //"math"
)

func STV_SWF(p rad.Profile) (rad.Count, error){
  resultat := make(rad.Count)
  maj := len(p)/2 + 1
  n := len(p[0])
  var worst_alt rad.Alternative
  for n != 1 {
    voteMaj, _ := MajoritySWF(p)
    bestalt := GetMaxOfCount(voteMaj)
    // si le score  est majoritaire on s arrete la 
    if voteMaj[bestalt] >= maj{
      resultat[bestalt] = voteMaj[bestalt]
      return resultat, nil
    }
    worst_alt = GetMinOfCount(voteMaj)
    // on supprime l'alternative la moins aimée du profil
    p = UpdateProfile(p, worst_alt)
    n -=1
  }
  return resultat, nil
}

func STV_SCF(p rad.Profile) (bestAlts []rad.Alternative, err error){
  resultat, err := STV_SWF(p)
	bestAlts = maxCount(resultat)
	return bestAlts, nil
}
