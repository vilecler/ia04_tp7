package comsoc

import(
    rad "gitlab.utc.fr/lagruesy/restagentdemo"
)

func ApprovalSWF(p rad.Profile) (count rad.Count, err error){
  // les profils sont incomplets, les candidats non soutenus sont ceux qui ne sont pas dans les prefs
  resultat := make(rad.Count)
	for _, preferences := range(p){
    for _, alt := range(preferences){
      if _, ok := resultat[alt] ; !ok {
        resultat[alt] = 1
      }else{
        resultat[alt] = resultat[alt] + 1
      }
    }
  }
  return resultat, nil
}

func ApprovalSCF(p rad.Profile) (bestAlts []rad.Alternative, err error){
  resultat_pre, _ := ApprovalSWF(p)
  bestAlts = maxCount(resultat_pre)
  return bestAlts, nil
}
