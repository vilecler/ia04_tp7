package comsoc

import (
	"math/rand"

	rad "gitlab.utc.fr/lagruesy/restagentdemo"
)

func IdentityTieBreak(alt []rad.Alternative) rad.Count {
	resultat := make(rad.Count)
	for i, alt := range alt {
		resultat[alt] = i
	}
	return resultat
}

func TossACoinToYourWitcher(alt []rad.Alternative) rad.Alternative {
	i := rand.Intn(len(alt) - 1)
	return alt[i]
}
