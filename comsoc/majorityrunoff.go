package comsoc

import(
  rad "gitlab.utc.fr/lagruesy/restagentdemo";
  "fmt"
)

func MajorityRunOffSWF(p rad.Profile) (count rad.Count, err error){
  resultat := make(rad.Count)
  n := 2
  var worst_alt rad.Alternative
  // comme stv mais pour n = 2
  for n != 0 {
    //resultat := make(rad.Count)
    resultat, _ = MajoritySWF(p)
    worst_alt = GetMinOfCount(resultat)
    p = UpdateProfile(p, worst_alt)
    n -=1
  }
  fmt.Println(resultat)
  return resultat, nil
}

func MajorityRunOffSCF(p rad.Profile) (bestAlts []rad.Alternative, err error){
  resultat, err := MajorityRunOffSWF(p)
	bestAlts = maxCount(resultat)
	return bestAlts, nil
}
