package comsoc

import(
    rad "gitlab.utc.fr/lagruesy/restagentdemo"
)

func BordaSWF(p rad.Profile) (rad.Count, error){
  resultat := make(rad.Count)
	for _, preferences := range(p){
      for i, alt := range(preferences){
        if _, ok := resultat[alt] ; !ok {
          // les alternatives sont classées de la préférée à la moins appréciée.  
    			resultat[alt] = len(preferences) - i
    		}else{
    			resultat[alt] = resultat[alt] + len(preferences) -i
    		}
      }
  }
  return resultat, nil
}

func BordaSCF(p rad.Profile) (bestAlts []rad.Alternative, err error) {
  resultat_pre,_ := BordaSWF(p)
	bestAlts = maxCount(resultat_pre)
  return bestAlts, nil
}
