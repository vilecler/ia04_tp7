package restserveragent

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"sync"

	rad "gitlab.utc.fr/lagruesy/restagentdemo"
	comsoc "gitlab.utc.fr/lagruesy/restagentdemo/comsoc"
)

//VoteRequestHandler struct
type VoteRequestHandler struct {
	sync.Mutex
	Type  string        //Type of the VoteRequestHandler
	Votes []comsoc.Vote //Votes managed by this VoteRequestHandler
}

//VoteRequestHandler constructor
func NewVoteRequestHandler(url string) *VoteRequestHandler {
	vh := VoteRequestHandler{}
	vh.Type = url
	return &vh
}

//Returns a vote matching a specific ID in this handler context. (vh.Votes)
func (vh *VoteRequestHandler) getVoteByID(ID string) *comsoc.Vote {
	for i := 0; i < len(vh.Votes); i++ {
		if vh.Votes[i].ID == ID {
			return &vh.Votes[i]
		}
	}
	return nil
}

//Checking method
func (vh *VoteRequestHandler) checkMethod(method string, w http.ResponseWriter, r *http.Request) bool {
	if r.Method != method {
		fmt.Fprintf(w, "method %q not allowed", r.Method)
		return false
	}
	return true
}

//Get RequestPost obect from Request
func (*VoteRequestHandler) decodeRequestPost(r *http.Request) (req rad.RequestPost, err error) {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	err = json.Unmarshal(buf.Bytes(), &req)
	return
}

//Get RequestPut obect from Request
func (*VoteRequestHandler) decodeRequestPut(r *http.Request) (req rad.RequestPut, err error) {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	err = json.Unmarshal(buf.Bytes(), &req)
	return
}

//Get RequestGet obect from Request
func (*VoteRequestHandler) decodeRequestGet(r *http.Request) (req rad.RequestGet, err error) {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	err = json.Unmarshal(buf.Bytes(), &req)
	return
}

/*
 *	Fonction HandlePost:
 *
 *		Permet de créer un vote selon le type du VoteRequestHandler.
 *		Pour les requêtes de type /vote_type/create. Paramètre RequestPost objet.
 */
func (vh *VoteRequestHandler) HandlePost(w http.ResponseWriter, r *http.Request) {
	vh.Lock()
	defer vh.Unlock()

	req, err := vh.decodeRequestPost(r) //Decode request
	if err != nil {
		fmt.Println("Error first occured:", err)
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, err.Error())
		return
	}

	if !vh.checkMethod("POST", w, r) { //Check request method
		return
	}

	//Creating vote
	alternatives := req.Alternatives
	votersNumber := req.VotersNumber
	vote := comsoc.NewVote(alternatives, votersNumber, vh.Type) //creating vote

	vh.Votes = append(vh.Votes, *vote) //appending new vote

	//Handle response
	var resp rad.ResponsePost
	resp.ID = vote.ID //Return ID of the vote

	w.WriteHeader(http.StatusOK)
	serial, erro := json.Marshal(resp)
	if erro != nil {
		fmt.Println("Error occured:", err)
	}
	w.Write(serial) //Send response
}

/*
 *	Fonction HandlePut:
 *
 *		Permet de voter un vote selon le type du VoteRequestHandler.
 *		Pour les requêtes de type /vote_type/vote. Paramètre RequestPut objet.
 */
func (vh *VoteRequestHandler) HandlePut(w http.ResponseWriter, r *http.Request) {
	vh.Lock()
	defer vh.Unlock()

	req, err := vh.decodeRequestPut(r) //Decode request
	if err != nil {
		fmt.Println("An error has occured while voting from server:", err)
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, err.Error())
		return
	}

	if !vh.checkMethod("PUT", w, r) { //Check request method
		return
	}

	vote := vh.getVoteByID(req.VoteID) //Try to retrieve vote by ID

	var resp rad.ResponsePut //Response object
	if vote == nil {         //This vote does not exist
		resp.Result = 0
		goto end
	}

	//Check agent has not voted yet
	if vote.HasAgentVoted(req.ID) == 1 { //agent has already voted
		resp.Result = 0
		goto end
	}

	//Check if preferences are acceptable
	if vh.Type != "Approval" {
		if len(req.Prefs) != len(vote.Alternatives) {
			resp.Result = 0
			goto end
		}
		for i := 0; i < len(vote.Alternatives); i++ { //check if each alternative occurs in agent preferences
			found := false
			for j := 0; j < len(req.Prefs); j++ {
				found = true
			}
			if !found {
				resp.Result = 0
				goto end
			}
		}
	}

	//Add alternatives to profile
	vote.AgentVote(req.ID, req.Prefs)
	resp.Result = 1

end:
	w.WriteHeader(http.StatusOK)
	serial, _ := json.Marshal(resp)
	w.Write(serial) //Write response
}

/*
 *	Fonction HandlePut:
 *
 *		Permet de récupérer les résultats d'un vote selon le type du VoteRequestHandler.
 *		Pour les requêtes de type /vote_type/results. Paramètre RequestGet objet.
 *		Les résultats sont calculés pour la premières requêtes seulement et mise en cache dans l'objet Vote correspondant de manière à ne pas faire de calculs inutiles.
 *		Appelle l'algorithme de vote correspondant au type de VoteRequestHandler
 */
func (vh *VoteRequestHandler) HandleGet(w http.ResponseWriter, r *http.Request) {
	vh.Lock()
	defer vh.Unlock()

	ID := r.URL.Query().Get("vote_id") //Get vote ID from request

	if !vh.checkMethod("GET", w, r) { //Check method type
		return
	}

	var resp rad.ResponseGet   //Reponse object
	vote := vh.getVoteByID(ID) //Try to get vote by ID

	if vote == nil { //This vote does not exist
		goto end
	}

	if len(vote.Results) != 0 { //If results is in cache send result
		resp.Alternatives = vote.Results //Results already calculated
		goto end
	}

	//Check vote is closed
	if vote.GetState() == 0 {
		goto end
	}

	switch vh.Type { //Call algorithm depending on the type
	//Implemented : "Approval", "Borda", "Condorcet", "Copeland", "Simple Majority", "Majority Run Off", "STV"
	case "Approval":
		bestAlts, err := comsoc.ApprovalSCF(vote.Profile)
		if err != nil {
			goto end
		}
		vote.Results = bestAlts
	case "Borda":
		bestAlts, err := comsoc.BordaSCF(vote.Profile)
		if err != nil {
			goto end
		}
		vote.Results = bestAlts
	case "Condorcet":
		bestAlt := comsoc.CondorcetWinner(vote.Profile)
		vote.Results = []rad.Alternative{bestAlt} //Condorcet Algorithm only returns the best alternative
	case "Copeland":
		bestAlts, err := comsoc.CopelandSCF(vote.Profile)
		if err != nil {
			goto end
		}
		vote.Results = bestAlts
	case "Simple Majority":
		bestAlts, err := comsoc.MajoritySCF(vote.Profile)
		if err != nil {
			goto end
		}
		vote.Results = bestAlts
	case "Majority Run Off":
		bestAlts, err := comsoc.MajorityRunOffSCF(vote.Profile)
		if err != nil {
			goto end
		}
		vote.Results = bestAlts
	case "STV":
		bestAlts, err := comsoc.STV_SCF(vote.Profile)
		if err != nil {
			goto end
		}
		vote.Results = bestAlts
	}

	if len(vote.Results) > 1 { //TieBreak is required here
		vote.Results = []rad.Alternative{comsoc.TossACoinToYourWitcher(vote.Results)} //Use random, selection
	}

	resp.Alternatives = vote.Results

end:
	w.WriteHeader(http.StatusOK)
	serial, _ := json.Marshal(resp)
	w.Write(serial) //Send response
}
