package comsoc

import (
  "testing";
  //  "fmt";
  "reflect"
  rad "gitlab.utc.fr/lagruesy/restagentdemo";
)

func TestCopelandSWF(t *testing.T){
  var prof [][]rad.Alternative
  voter1 := []rad.Alternative{1,2,3}
  voter2 := []rad.Alternative{2,3,1}
  voter3 := []rad.Alternative{1,2,3}
  prof = append(prof, voter1, voter2, voter3)
  got, _ := CopelandSWF(prof)
  want := map[rad.Alternative]int{
      1: 2,
      2: 0,
      3: -2,
  }
  echec := false
  for alt, val := range(got){
    if val != want[alt]{
      echec = true
    }
  }
  if echec {
      t.Errorf("On a %d alors que l'on veut %d etant donne, %v", got, want, prof)
    }
}


func TestCopelandSCF(t *testing.T){
  var prof [][]rad.Alternative
  voter1 := []rad.Alternative{1,2,3}
  voter2 := []rad.Alternative{2,3,1}
  voter3 := []rad.Alternative{1,2,3}
  prof = append(prof, voter1, voter2, voter3)
  got, _ := CopelandSCF(prof)
  want := []rad.Alternative{1}
  if !( reflect.DeepEqual(got, want)) {
    t.Errorf("On a %d  alors que l'on veut %d etant donne, %v", got, want,   prof)
  }
}
