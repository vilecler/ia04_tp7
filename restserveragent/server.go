package restserveragent

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"strings"
	"sync"
	"time"

	rad "gitlab.utc.fr/lagruesy/restagentdemo"
	comsoc "gitlab.utc.fr/lagruesy/restagentdemo/comsoc"
)

//RestServerAgent struct
type RestServerAgent struct {
	sync.Mutex
	id   string
	addr string

	VoteRequestHandlers []VoteRequestHandler //All the VoteRequestHandlers
}

//RestServerAgent constructor
func NewRestServerAgent(addr string) *RestServerAgent {
	return &RestServerAgent{id: addr, addr: addr}
}

// Test de la méthode
func (rsa *RestServerAgent) checkMethod(method string, w http.ResponseWriter, r *http.Request) bool {
	if r.Method != method {
		//w.WriteHeader(http.StatusMethodNotAllowed)
		fmt.Fprintf(w, "method %q not allowed", r.Method)
		return false
	}
	return true
}

/*
 * Fonction handleVotes:
 *
 *		Retourne l'ensemble des votes sous la forme ProtectedVote pour raison de sécurité en fonction de l'agent qui demande.
 */
func (rsa *RestServerAgent) handleVotes(w http.ResponseWriter, r *http.Request) {
	rsa.Lock()
	defer rsa.Unlock()

	ID, _ := strconv.Atoi(r.URL.Query()["ID"][0]) //Get Agent ID from GET request

	var count int //Count number of votes in each handle and sum
	for i := 0; i < len(rsa.VoteRequestHandlers); i++ {
		count = count + len(rsa.VoteRequestHandlers[i].Votes)
	}

	votesGlobal := make([]comsoc.Vote, count) //All the votes
	votes := make([]rad.ProtectedVote, count) //List of protected votes

	var c int //Get all votes
	for i := 0; i < len(rsa.VoteRequestHandlers); i++ {
		for j := 0; j < len(rsa.VoteRequestHandlers[i].Votes); j++ {
			votesGlobal[c] = rsa.VoteRequestHandlers[i].Votes[j]
			c++
		}
	}

	for i := 0; i < len(votesGlobal); i++ { //For each Vote cast to ProtectedVote
		var voteOpenned int

		if votesGlobal[i].GetState() == 0 { //Set if vote is opened or not
			voteOpenned = 1
		}

		votes[i] = rad.ProtectedVote{ //Cast Vote to pretoected vote
			Alternatives: votesGlobal[i].Alternatives,
			HasVoted:     votesGlobal[i].HasAgentVoted(rad.AgentID(ID)),
			Open:         voteOpenned,
			Type:         votesGlobal[i].Type,
			ID:           votesGlobal[i].ID,
			Results:      votesGlobal[i].Results,
		}
	}

	//Handle response
	var resp rad.ResponseAll
	resp.Votes = votes

	w.WriteHeader(http.StatusOK)
	serial, _ := json.Marshal(resp)
	w.Write(serial) //Sending response
}

/*
 * Fonction Start:
 *
 *		Permet de démarrer le serveur qui écoute sur /all GET et qui démarre un VoteRequestHandler pour chaque type de vote implémentée dans Types
 */
func (rsa *RestServerAgent) Start() {
	mux := http.NewServeMux()

	mux.HandleFunc("/all", rsa.handleVotes) //Handle /all GET

	//Create all VoteRequestHandlers
	for i := 0; i < len(rad.GetSupportedTypes()); i++ {
		rsa.VoteRequestHandlers = append(rsa.VoteRequestHandlers, *NewVoteRequestHandler(rad.GetSupportedTypes()[i]))
	}

	//Each VoteRequestHandler will be Listening to specific URLs depending of its type -> /vote_type/create POST  /vote_type/vote PUT  /vote_type/results GET
	for i := 0; i < len(rsa.VoteRequestHandlers); i++ {
		typeURL := strings.ReplaceAll(strings.ToLower(rsa.VoteRequestHandlers[i].Type), " ", "")
		urlCreate := "/" + typeURL + "/create"
		mux.HandleFunc(urlCreate, rsa.VoteRequestHandlers[i].HandlePost)
		urlVote := "/" + typeURL + "/vote"
		mux.HandleFunc(urlVote, rsa.VoteRequestHandlers[i].HandlePut)
		urlResults := "/" + typeURL + "/results"
		mux.HandleFunc(urlResults, rsa.VoteRequestHandlers[i].HandleGet)
	}

	// Creating HTTP server
	s := &http.Server{
		Addr:           rsa.addr,
		Handler:        mux,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}

	//Launching server
	log.Println("Listening on", rsa.addr)
	go log.Fatal(s.ListenAndServe())
}
