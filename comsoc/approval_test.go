package comsoc

import (
  "testing";
  "reflect";

  rad "gitlab.utc.fr/lagruesy/restagentdemo";
)

func TestApprovalSWF(t *testing.T){
  //func BordaSWF(p Profile) (Count, error){
  var prof [][]rad.Alternative
  voter1 := []rad.Alternative{1,2,3}
  voter2 := []rad.Alternative{2,1}
  voter3 := []rad.Alternative{1}
  prof = append(prof, voter1, voter2, voter3)
  got, _ := ApprovalSWF(prof)
  want := map[rad.Alternative]int{
    1: 3,
    2: 2,
    3: 1,
}

  for alt, val := range(got){
    if val != want[alt]{
      t.Errorf("On a %d alors que l'on veut %d etant donne, %v", got, want, prof)
    }
  }
}


func TestApprovalSCF(t *testing.T){
  var prof [][]rad.Alternative
  voter1 := []rad.Alternative{1,2,3}
  voter2 := []rad.Alternative{2,1}
  voter3 := []rad.Alternative{1}
  prof = append(prof, voter1, voter2, voter3)
  got, _ := ApprovalSCF(prof)
  want := []rad.Alternative{1}
  if !( reflect.DeepEqual(got, want)) {
    t.Errorf("On a %d  alors que l'on veut %d etant donne, %v", got, want,   prof)
  }
}
