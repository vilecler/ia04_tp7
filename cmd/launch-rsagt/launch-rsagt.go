package main

import (
	"fmt"

	ras "gitlab.utc.fr/lagruesy/restagentdemo/restserveragent"
)

func main() {
	server := ras.NewRestServerAgent(":8000")
	server.Start()
	fmt.Scanln()
}
