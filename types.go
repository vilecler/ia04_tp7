package restagentdemo

//Retourne de manière constante la liste de méthodes de vote implémentées
func GetSupportedTypes() []string {
	return []string{"Approval", "Borda", "Condorcet", "Copeland", "Simple Majority", "Majority Run Off", "STV"}
}

type AgentID int

type Alternative int
type Profile [][]Alternative
type Count map[Alternative]int

//Structure pour ne pas transmettre les votes de tout le monde aux agents
type ProtectedVote struct {
	Alternatives []Alternative //Liste des alternatives disponibles
	HasVoted     int           //Si l'agent a voté ou non
	Open         int           //Si le vote est ouvert ou non
	Type         string        //Type de vote : majorité

	ID string //Identifiant unique du vote

	Results []Alternative //Résultat du vote, liste des alternatives classées par ordre décroissant
}

//REQUEST TYPES
//Pour créer un vote, le type de vote est dans l'URL /vote_type/create
type RequestPost struct {
	VotersNumber int           `json:"votersnumber"` //Nombre de votants
	Alternatives []Alternative `json:"alternatives"` //Liste des alternatives
}

//Pour voter
type RequestPut struct {
	ID     AgentID       `json:"id_agent"` //Identifiant de l'agent qui vote
	Prefs  []Alternative `json:"prefs"`    //Préférences de l'agent
	VoteID string        `json:"vote_id"`  //Identifiant du vote auquel l'agent vote
}

//Pour obtenir les résultats d'un vote doit fournir l'ID du vote
type RequestGet struct {
	VoteID string `json:"vote_id"`
}

//Pour obtenir tous les votes doit fournir l'ID de l'agent qui fait la requête
type RequestAll struct {
	ID AgentID `json:"id_agent"`
}

//Pour la créqtion de vote renvoie l'ID du vote
type ResponsePost struct {
	ID string `json:"vote_id"`
}

//Pour le vote renvoir 1 si ok 0 si ko
type ResponsePut struct {
	Result int `json:"res"`
}

//Pour les résultats d'un vote renvoie les résultats
type ResponseGet struct {
	Alternatives []Alternative `json:"alternatives"`
}

//Pour obtenir tous les votes, renvoie la liste des votes sous la forme ProtectedVote
type ResponseAll struct {
	Votes []ProtectedVote `json:"votes"`
}
