package comsoc

import (
	b64 "encoding/base64"
	"fmt"
	"time"

	rad "gitlab.utc.fr/lagruesy/restagentdemo"
)

type Vote struct {
	Alternatives []rad.Alternative //Liste des alternatives disponibles
	HasVoted     []int             //Tableau des agents ayant voté
	Profile      rad.Profile       //Liste des préférences de tous les agents
	Type         string            //Type de vote : majorité

	ID string //Identifiant unique du vote

	Results []rad.Alternative //Résultat du vote, liste des alternatives classées par ordre décroissant
}

func NewVote(alternatives []rad.Alternative, n int, votingType string) *Vote {
	var vote Vote

	vote.Alternatives = alternatives
	vote.HasVoted = make([]int, n)
	vote.Profile = make(rad.Profile, n)

	vote.Type = votingType

	//Generate vote ID
	t := time.Now()
	vote.ID = b64.StdEncoding.EncodeToString([]byte(t.String()))

	return &vote
}

/*  Function GetState()
 *
 *  Retourne 1 si le vote est fini, 0 sinon
 */
func (v *Vote) GetState() int {
	for i := 0; i < len(v.HasVoted); i++ {
		if v.HasVoted[i] != 1 {
			return 0
		}
	}
	return 1
}

/*  Function HasAgentVoted()
 *
 *  Retourne 1 si l'agent a voté, 0 sinon
 */
func (v *Vote) HasAgentVoted(id rad.AgentID) int {
	return v.HasVoted[id]
}

/*  Function AgentVote()
 *
 *  L'agent vote avec son ID et ses préférences
 */
func (v *Vote) AgentVote(id rad.AgentID, prefs []rad.Alternative) {
	v.Profile[id] = prefs
	v.SetAgentHasVoted(id)
}

/*  Function SetAgentHasVoted()
 *
 *  Indique qu'un agent a voté
 */
func (v *Vote) SetAgentHasVoted(id rad.AgentID) {
	v.HasVoted[id] = 1
}

func (v *Vote) Debug() {
	fmt.Println("Debug Vote:")
	fmt.Println("ID:", v.ID)
	fmt.Println("Alternatives:", v.Alternatives)
	fmt.Println("HasVoted:", v.HasVoted)
	fmt.Println("Profile:", v.Profile)
	fmt.Println("Type:", v.Type)
	fmt.Println("Results:", v.Results)
}
