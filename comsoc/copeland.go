package comsoc

import (
  rad "gitlab.utc.fr/lagruesy/restagentdemo";
  //"fmt"
)

func CopelandSWF(p rad.Profile) (rad.Count, error){
  var arr_candidats []rad.Alternative
  var arr_candidats2 []rad.Alternative
  score_candidats := make(rad.Count)
  var iter_cand rad.Alternative
  var sum_iter_cand int
  for _ ,alt := range(p[0]){
    score_candidats[alt] = 0
    arr_candidats = append(arr_candidats, alt)
    arr_candidats2 = append(arr_candidats2, alt)
    }
  // Comparaison deux à deux des candidats
  for len(arr_candidats) != 0 {
    iter_cand = arr_candidats[0]
    arr_candidats = arr_candidats[1:]
    for _, alt2 := range(arr_candidats2){
        sum_iter_cand = 0
        for _,tab := range(p){
          add := pref_alt(iter_cand, alt2, tab)
          sum_iter_cand += add
          }
          if sum_iter_cand > 0{
            score_candidats[iter_cand] += 1
          }else if sum_iter_cand <0{
            score_candidats[iter_cand] += -1
          }
      }
    }
      return score_candidats ,nil
}

func CopelandSCF(p rad.Profile) (bestAlts []rad.Alternative, err error) {
  resultat, err := CopelandSWF(p)
	bestAlts = maxCount(resultat)
	return bestAlts, nil
}
